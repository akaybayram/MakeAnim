const rl = @import("raylib");
const std = @import("std");
const fmt = std.fmt;

const Config = struct { ZOOM_RATE_CHANGE_SPEED: f32 = 10 };
const DrawUtils = struct {
    const DrawData = struct {
        texture: rl.Texture2D = undefined,
        position: rl.Vector2 = rl.Vector2{ .x = 0, .y = 0 },
        source_rect: rl.Rectangle = rl.Rectangle{ .x = 0, .y = 0, .width = 0, .height = 0 },
        bound_rect: rl.Rectangle = rl.Rectangle{ .x = 0, .y = 0, .width = 0, .height = 0 },
        origin: rl.Vector2 = rl.Vector2{ .x = 0, .y = 0 },
        tint: rl.Color = rl.Color.white,
    };

    const FramedDataFrame = struct {
        target: *DrawData,
        rows: u32,
        columns: u32,
    };

    const DrawDataFrame = struct {
        target: *FramedDataFrame,
        source_rect: rl.Rectangle,
    };

    fn init_data(data: *DrawData, texture: rl.Texture2D) void {
        data.texture = texture;
        data.source_rect = rl.Rectangle{ .x = 0, .y = 0, .width = @floatFromInt(data.texture.width), .height = @floatFromInt(data.texture.height) };
    }

    fn deinit_data(data: *DrawData) void {
        rl.unloadTexture(data.texture);
    }

    fn draw_data(data: *DrawData) void {
        rl.drawTexturePro(data.texture, data.source_rect, data.bound_rect, rl.Vector2{ .x = 0, .y = 0 }, 0, rl.Color.white);
    }

    fn draw_frame(data: *DrawDataFrame) void {
        rl.drawTexturePro(data.target.texture, data.source_rect, data.target.bound_rect, rl.Vector2{ .x = 0, .y = 0 }, 0, rl.Color.white);
    }

    fn slice_draw_data(data: *DrawData, row: u32, col: u32) FramedDataFrame {
        return FramedDataFrame{
            .target = data,
            .rows = row,
            .columns = col,
        };
    }

    fn get_draw_data_slice(data: *FramedDataFrame, idx: u32) DrawDataFrame {
        const sel_row = (idx % data.rows);
        const sel_col = @divTrunc(idx, data.rows);
        const width = @divTrunc(data.source_rect.width, sel_row);
        const height = @divTrunc(data.source_rect.height, sel_col);

        return DrawDataFrame{
            .target = data,
            .source_rect = rl.Rectangle{ .x = sel_row * width, .y = sel_col * height },
        };
    }
};

const Target = struct {
    draw_data: DrawUtils.DrawData = DrawUtils.DrawData{},
    mouse_move_handle_offset: rl.Vector2 = rl.Vector2{ .x = 0, .y = 0 },
    zoom_rate: f32 = 1.0,
    is_move_mode: bool = false,

    fn update(self: *Target) void {
        const data = &get_gl_data().data;
        const config = &get_gl_data().config;
        var grid = &get_gl_data().grid;

        if (rl.isKeyPressed(.key_q)) {
            grid.vertical_line_count += 1;
        } else if (rl.isKeyPressed(.key_a) and grid.vertical_line_count > 1) {
            grid.vertical_line_count -= 1;
        }

        if (rl.isKeyPressed(.key_w)) {
            grid.horizontal_line_count += 1;
        } else if (rl.isKeyPressed(.key_s) and grid.horizontal_line_count > 1) {
            grid.horizontal_line_count -= 1;
        }

        if (data.mouse_wheel_movement.y != 0) {
            self.zoom_rate += config.ZOOM_RATE_CHANGE_SPEED * data.mouse_wheel_movement.y * data.dt;
            if (self.zoom_rate < 1.0) {
                self.zoom_rate = 1.0;
            }
        }

        if (rl.isMouseButtonPressed(rl.MouseButton.mouse_button_middle)) {
            if (rl.checkCollisionPointRec(data.mouse_position, self.draw_data.bound_rect)) {
                self.is_move_mode = true;
                self.mouse_move_handle_offset = rl.Vector2{ .x = data.mouse_position.x - self.draw_data.position.x, .y = data.mouse_position.y - self.draw_data.position.y };
            }
        }

        if (rl.isMouseButtonReleased(rl.MouseButton.mouse_button_middle)) {
            self.is_move_mode = false;
        }

        if (self.is_move_mode) {
            self.draw_data.position = rl.Vector2{ .x = data.mouse_position.x - self.mouse_move_handle_offset.x, .y = data.mouse_position.y - self.mouse_move_handle_offset.y };
        }

        self.draw_data.bound_rect = rl.Rectangle{
            .x = self.draw_data.position.x,
            .y = self.draw_data.position.y,
            .width = @as(f32, @floatFromInt(self.draw_data.texture.width)) * self.zoom_rate,
            .height = @as(f32, @floatFromInt(self.draw_data.texture.height)) * self.zoom_rate,
        };
    }
};

const GameData = struct {
    mouse_position: rl.Vector2 = undefined,
    mouse_wheel_movement: rl.Vector2 = undefined,
    dt: f32 = undefined,

    fn update(self: *GameData) void {
        self.mouse_position = rl.getMousePosition();
        self.mouse_wheel_movement = rl.getMouseWheelMoveV();
        self.dt = rl.getFrameTime();
    }
};

const TargetGrid = struct {
    vertical_line_count: u32 = 1,
    horizontal_line_count: u32 = 1,
    vertical_line_space: f32 = 0,
    horizontal_line_space: f32 = 0,
    horizontal_line_len: f32 = 0,
    vertical_line_len: f32 = 0,
    horizontal_image_count: u32 = 1,
    vertical_image_count: u32 = 1,
    target: *Target,

    fn update(self: *TargetGrid) void {
        self.horizontal_line_space = @as(f32, @floatFromInt(self.target.draw_data.texture.height)) * self.target.zoom_rate / @as(f32, @floatFromInt(self.horizontal_line_count));
        self.vertical_line_space = @as(f32, @floatFromInt(self.target.draw_data.texture.width)) * self.target.zoom_rate /
            @as(f32, @floatFromInt(self.vertical_line_count));
        self.horizontal_image_count = self.vertical_line_count;
        self.vertical_image_count = self.horizontal_line_count;
        self.vertical_line_len = @as(f32, @floatFromInt(self.target.draw_data.texture.height)) * self.target.zoom_rate;
        self.horizontal_line_len = @as(f32, @floatFromInt(self.target.draw_data.texture.width)) * self.target.zoom_rate;
    }

    fn draw(self: *TargetGrid) void {
        var line_start_pos = rl.Vector2{ .x = self.target.draw_data.position.x, .y = self.target.draw_data.position.y };
        var line_end_pos = rl.Vector2{ .x = self.horizontal_line_len + self.target.draw_data.position.x, .y = self.target.draw_data.position.y };
        for (0..self.horizontal_line_count) |_| {
            rl.drawLineEx(line_start_pos, line_end_pos, 2, rl.Color.yellow);
            line_start_pos.y += self.horizontal_line_space;
            line_end_pos.y += self.horizontal_line_space;
        }

        line_start_pos = rl.Vector2{ .x = self.target.draw_data.position.x, .y = self.target.draw_data.position.y };
        line_end_pos = rl.Vector2{ .x = self.target.draw_data.position.x, .y = self.vertical_line_len + self.target.draw_data.position.y };
        for (0..self.vertical_line_count) |_| {
            rl.drawLineEx(line_start_pos, line_end_pos, 2, rl.Color.yellow);
            line_start_pos.x += self.vertical_line_space;
            line_end_pos.x += self.vertical_line_space;
        }
    }
};

const MakeAnim = struct {
    target: Target = Target{},
    config: Config = Config{},
    data: GameData = GameData{},
    grid: TargetGrid = undefined,
};

var global_program_data = MakeAnim{};

fn get_gl_data() *MakeAnim {
    return &global_program_data;
}

fn draw_statistics() void {
    const target = get_gl_data().target;
    var buf: [256]u8 = undefined;
    var text_res = fmt.bufPrintZ(&buf, "target.bound_rect = [ x = {d:.2}, y = {d:.2}, w = {d:.2}, h = {d:.2} ]", .{
        target.draw_data.bound_rect.x,
        target.draw_data.bound_rect.y,
        target.draw_data.bound_rect.width,
        target.draw_data.bound_rect.height,
    });

    if (text_res) |text| {
        rl.drawText(text, 0, 0, 20, rl.Color.white);
    } else |err| {
        std.debug.print("buffer write error = {s}\n", .{@errorName(err)});
    }
}

fn update() void {
    var target = &get_gl_data().target;
    var data = &get_gl_data().data;
    var grid = &get_gl_data().grid;
    data.update();
    target.update();
    grid.update();
}

fn draw() void {
    var target = &get_gl_data().target;
    var grid = &get_gl_data().grid;
    DrawUtils.draw_data(&target.draw_data);
    grid.draw();

    draw_statistics();
}

fn init() void {
    var target = &get_gl_data().target;
    var grid = &get_gl_data().grid;
    DrawUtils.init_draw_data(&target.draw_data, rl.loadTexture("assets/hero.png"));
    grid.target = target;
    grid.vertical_line_count = 1;
    grid.horizontal_line_count = 1;
}

fn deinit() void {
    var gl_data = get_gl_data();
    DrawUtils.deinit_draw_data(&gl_data.target.draw_data);
}

pub fn main() anyerror!void {
    const screen_width = 1280;
    const screen_height = 720;

    rl.initWindow(screen_width, screen_height, "MakeAnim");
    defer rl.closeWindow();

    init();
    defer deinit();

    rl.setTargetFPS(60);

    while (!rl.windowShouldClose()) {
        update();
        rl.beginDrawing();
        defer rl.endDrawing();
        rl.clearBackground(rl.Color.blue);
        draw();
    }
}
